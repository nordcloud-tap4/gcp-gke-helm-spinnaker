variable "project" {
  description = "The ID of the project to apply any resources to."
}

variable "region" {
  description = "Default region"
}

variable "zone" {
  description = "Default zone"
}

variable "depends_on" {
  default     = []
  type        = "list"
  description = "Hack for expressing module to module dependency"
}

variable "vpc" {
  description = "Default VPC"
  default     = "default"
}

variable "spinnaker_service_account" {
  description = "Service account for Spinnaker"
  default     = "spinnaker-account"
}

variable "spinnaker_cluster_name" {
  description = "Spinnaker Cluster Name"
  default     = "spinnaker-tutorial"
}

variable "initial_node_count" {
  default     = 3
  description = "The number of nodes to create in this cluster (not including the Kubernetes master). Default to 3"
}

variable "min_node_count" {
  default     = 1
  description = "Minimum number of nodes in the NodePool. Must be >=1 and <= max_node_count Default to 1"
}

variable "max_node_count" {
  default     = 3
  description = "Maximum number of nodes in the NodePool. Must be >= min_node_count. Default to 3"
}

variable "disk_size_gb" {
  default = 50
}

variable "preemptible" {
  default     = true
  description = "A boolean that represents whether or not the underlying node VMs are preemptible. See the official documentation for more information. Defaults to true."
}

variable "machine_type" {
  default     = "n1-standard-1"
  description = "The name of a Google Compute Engine machine type. Defaults to n1-standard-4"
}

variable "min_master_version" {
  default     = "1.10.5-gke.3"
  description = "The minimum version of the master"
}

variable "tags" {
  type        = "list"
  default     = []
  description = "The list of instance tags applied to all nodes. Tags are used to identify valid sources or targets for network firewalls."
}

variable "service_account_key" {
  description = "Service account Key"
  default     = "spinnaker-sa.json"
}
variable "tiller_service_account" {
  description = "Service account for Tiller"
  default     = "tiller"
}

variable "tiller_namespace" {
  description = "Namespace for Tiller"
  default     = "kube-system"
}

variable "helm_version" {
  description = "Helm Version"
}
