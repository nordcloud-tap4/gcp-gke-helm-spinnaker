provider "google" {
  version = "~> 2.5.1"
  project = "${var.project}"
}

provider "google-beta" {
  version = "~> 2.5.1"
  project = "${var.project}"
}

# current config profile
data "google_client_config" "current" {}

## TODO: Enable following APIs
#container.googleapis.com
#compute.googleapis.com

# Create a GKE cluster to deploy Spinnaker
resource "google_container_cluster" "spinnaker_gke_cluster" {
  name               = "${var.spinnaker_cluster_name}"
  project            = "${var.project}"
  location           = "${var.zone}"
  network            = "${var.vpc}"

  # https://github.com/mcuadros/terraform-provider-helm/issues/56
  # https://github.com/terraform-providers/terraform-provider-kubernetes/pull/73
  # ABAC = true / RBAC
  enable_legacy_abac = true

  node_pool {
    initial_node_count = 3 #${var.initial_node_count} # var is giving error of illegal char
    node_config {
      machine_type = "${var.machine_type}"
    }
  }
}

resource "google_service_account" "spinnaker_service_account" {
  account_id   = "${var.spinnaker_service_account}"
  display_name = "${var.spinnaker_service_account}"
}

resource "google_project_iam_binding" "spinnaker_service_account_storage_admin" {
  role               = "roles/storage.admin"
  members            = [
    "serviceAccount:${google_service_account.spinnaker_service_account.email}"
  ]
}

# create spinnaker service accout key (json)
resource "google_service_account_key" "spinnaker_service_account_key" {
  service_account_id = "${google_service_account.spinnaker_service_account.name}"
}

# Topic
resource "google_pubsub_topic" "my_topic" {
  name    = "gcr"
  project = "${var.project}"
}

# Subscription for Topic
resource "google_pubsub_subscription" "my_topic_subscr" {
  name    = "gcr-triggers"
  project = "${var.project}"
  topic   = "${google_pubsub_topic.my_topic.name}"
}

# Make Spinnaker Service Account subscriber to the topic
resource "google_pubsub_subscription_iam_binding" "my_topic_subscr_iam" {
  subscription = "${google_pubsub_subscription.my_topic_subscr.name}"
  project      = "${var.project}"
  role         = "roles/pubsub.subscriber"
  members      = [
    "serviceAccount:${google_service_account.spinnaker_service_account.email}",
  ]
}

provider "kubernetes" {
  load_config_file       = false
  host                   = "${google_container_cluster.spinnaker_gke_cluster.endpoint}"
  token                  = "${data.google_client_config.current.access_token}"
  client_certificate     = "${base64decode(google_container_cluster.spinnaker_gke_cluster.master_auth.0.client_certificate)}"
  client_key             = "${base64decode(google_container_cluster.spinnaker_gke_cluster.master_auth.0.client_key)}"
  cluster_ca_certificate = "${base64decode(google_container_cluster.spinnaker_gke_cluster.master_auth.0.cluster_ca_certificate)}"
}

# TODO: cluster-admin role binding to be done to current user

# tiller account
resource "kubernetes_service_account" "tiller_service_account" {
  metadata {
    name      = "${var.tiller_service_account}"
    namespace = "${var.tiller_namespace}"
  }
}

# bind tiller account to cluster-admin
resource "kubernetes_cluster_role_binding" "tiller_admin_binding" {
  metadata {
    name = "tiller-admin-binding"
  }
  role_ref {
    kind      = "ClusterRole"
    name      = "cluster-admin"
    api_group = "rbac.authorization.k8s.io"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "${kubernetes_service_account.tiller_service_account.metadata.0.name}"
    namespace = "${var.tiller_namespace}"
    api_group = ""
    # See https://github.com/terraform-providers/terraform-provider-kubernetes/issues/204
  }
}

resource "kubernetes_cluster_role_binding" "spinnaker_admin" {
  metadata {
    name = "spinnaker-admin"
  }
  role_ref {
    kind      = "ClusterRole"
    name      = "cluster-admin"
    api_group = "rbac.authorization.k8s.io"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "default"
    namespace = "default"
    api_group = ""
  }
}

# helm setup and install tiller to k8s
provider "helm" {
  tiller_image    = "gcr.io/kubernetes-helm/tiller:${var.helm_version}"
  namespace       = "${var.tiller_namespace}"
  service_account = "${var.tiller_service_account}"

  # k8s part need to be repeated as above
  kubernetes {
    host                   = "${google_container_cluster.spinnaker_gke_cluster.endpoint}"
    token                  = "${data.google_client_config.current.access_token}"
    client_certificate     = "${base64decode(google_container_cluster.spinnaker_gke_cluster.master_auth.0.client_certificate)}"
    client_key             = "${base64decode(google_container_cluster.spinnaker_gke_cluster.master_auth.0.client_key)}"
    cluster_ca_certificate = "${base64decode(google_container_cluster.spinnaker_gke_cluster.master_auth.0.cluster_ca_certificate)}"
  }
}

# create GCS bucket
resource "google_storage_bucket" "spinnaker_config" {
  name          = "${var.project}-spinnaker-config"
  location      = "${var.region}"
  storage_class = "REGIONAL"
  force_destroy = "true"
}

# install spinnaker to k8s
resource "helm_release" "spinnaker" {
  name       = "spinnaker"
  chart      = "stable/spinnaker"
  version    = "1.8.1"
  timeout    = 600

  values = [<<EOF
gcs:
  enabled: true
  bucket: ${google_storage_bucket.spinnaker_config.name}
  project: ${var.project}
  jsonKey: '${base64decode(google_service_account_key.spinnaker_service_account_key.private_key)}'

dockerRegistries:
- name: gcr
  address: https://gcr.io
  username: _json_key
  password: '${base64decode(google_service_account_key.spinnaker_service_account_key.private_key)}'
  email: ${google_service_account.spinnaker_service_account.email}

minio:
  enabled: false

halyard:
  spinnakerVersion: 1.12.5
  image:
    repository: gcr.io/spinnaker-marketplace/halyard
    tag: 1.19.2
  additionalScripts:
    create: true
    data:
      enable_gcs_artifacts.sh: |-
        $HAL_COMMAND config artifact gcs account add gcs-${var.project} --json-path /opt/gcs/key.json
        $HAL_COMMAND config artifact gcs enable
      enable_pubsub_triggers.sh: |-
        $HAL_COMMAND config pubsub google enable
        $HAL_COMMAND config pubsub google subscription add gcr-triggers \
          --subscription-name gcr-triggers \
          --json-path /opt/gcs/key.json \
          --project ${var.project} \
          --message-format GCR
EOF
  ]

  depends_on = [
    "google_service_account_key.spinnaker_service_account_key",
    "google_storage_bucket.spinnaker_config"
  ]
}

