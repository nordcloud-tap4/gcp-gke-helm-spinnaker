// parameters
region = "europe-north1"
zone = "europe-north1-a"
project = "trump-spin-9"

vpc = "default"

// helm
helm_version = "v2.13.1"
