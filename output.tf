output "service_account_email" {
  value = "${google_service_account.spinnaker_service_account.email}"
}

output "service_account_name" {
  value = "${google_service_account.spinnaker_service_account.name}"
}

output "spinnaker_gke_cluster_name" {
  value = "${google_container_cluster.spinnaker_gke_cluster.name}"
}

output "spinnaker_gke_cluster_endpoint" {
  value = "${google_container_cluster.spinnaker_gke_cluster.endpoint}"
}
